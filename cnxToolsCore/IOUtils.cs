﻿using System.IO;

namespace cnxToolsCore {
    // ReSharper disable once InconsistentNaming
    public class IOUtils {
        public static FileStream FileCanRead(string filePath) {
            return File.Open(filePath, FileMode.Open, FileAccess.Read);
        }
    }
}
